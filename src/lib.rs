pub mod matrix;

pub use matrix::{Matrix, det, mat_mul};
