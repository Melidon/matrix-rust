use matrix::{det, mat_mul};

fn main() {
    let a = [[11, 3], [7, 11]];
    let b = [[8, 0, 1], [0, 3, 5]];
    let c = mat_mul(&a, &b);
    dbg!(c);

    let m = [[1, 3, 5, 9], [1, 3, 1, 7], [4, 3, 9, 7], [5, 2, 0, 9]];
    println!("Expected: {}; Calculated: {}", -376, det(m));
}
