use std::ops::{Add, Div, Mul, Neg, Sub};

pub type Matrix<T, const H: usize, const W: usize> = [[T; W]; H];

pub fn mat_mul<T, const H: usize, const WH: usize, const W: usize>(
    lhs: &Matrix<T, H, WH>,
    rhs: &Matrix<T, WH, W>,
) -> Matrix<T, H, W>
where
    T: Copy + Default + Add<Output = T> + Mul<Output = T>,
{
    let mut result = [[T::default(); W]; H];
    for y in 0..H {
        for x in 0..W {
            for i in 0..WH {
                result[y][x] = result[y][x] + lhs[y][i] * rhs[i][x];
            }
        }
    }
    result
}

// https://en.wikipedia.org/wiki/Bareiss_algorithm
pub fn det<T, const HW: usize>(mut matrix: Matrix<T, HW, HW>) -> T
where
    T: Copy
        + Default
        + PartialEq
        + Div<Output = T>
        + Mul<Output = T>
        + Neg<Output = T>
        + Sub<Output = T>,
{
    let mut negate_result = false;
    for k in 0..HW - 1 {
        if matrix[k][k] == T::default() {
            let mut swapped = false;
            let mut i = k;
            while i < HW && !swapped {
                if matrix[i][k] != T::default() {
                    for j in k..HW {
                        let tmp = matrix[k][j];
                        matrix[k][j] = matrix[i][j];
                        matrix[i][j] = tmp;
                    }
                    negate_result = !negate_result;
                    swapped = true;
                }
                i += 1;
            }
            if !swapped {
                return T::default();
            }
        }
        for i in k + 1..HW {
            for j in k + 1..HW {
                matrix[i][j] = matrix[i][j] * matrix[k][k] - matrix[i][k] * matrix[k][j];
                if k > 0 {
                    matrix[i][j] = matrix[i][j] / matrix[k - 1][k - 1];
                }
            }
        }
    }
    if negate_result {
        -matrix[HW - 1][HW - 1]
    } else {
        matrix[HW - 1][HW - 1]
    }
}
